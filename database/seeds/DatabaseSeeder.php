<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([array(
            'title' => 'category 1',
            'description' => 'New Category',
        )]);

        DB::table('posts')->insert([array(
            'title' => 'Post 1',
            'slug' => 'post_1',
            'description' => 'New Post 1',
            'category_id' => '1',
        )]);

        DB::table('pages')->insert([array(
            'title' => 'Page 1',
            'slug' => 'page_1',
            'description' => 'New page 1',
        )]);

        DB::table('pages')->insert([array(
            'title' => 'About',
            'slug' => 'about',
            'description' => 'About 11 1 1111 1 1',
        )]);

        DB::table('pages')->insert([array(
            'title' => 'Sample Post',
            'slug' => 'sample_post',
            'description' => 'Sample Post 12 121 12 122',
        )]);
    }
}

@extends('layout.layout')

<style>
    .display-comment .display-comment {
        margin-left: 40px
    }
</style>

@section('content')
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <img src="{{ Voyager::image( $post->image ) }}">

        <h2>{!! $post->title !!}</h2>
        <p>
            {!! $post->description !!}
        </p>

        <h3>List comments:</h3>

        @include('partials._comment_replies', ['comments' => $post->comments, 'post_id' => $post->id])

        <form class="contact_form" action="{{ route('comments.save') }}" method="post" name="contact_form">
            @csrf
            <ul>
                <li>
                    <h2>Add Comments</h2>
                    <span class="required_notification">* Denotes Required Field</span>
                </li>

                <input name="id" type="hidden" value="{{ $post->id }}">

                <li>
                    <label for="name">Name:</label>
                    <input name="author" type="text"  placeholder="John Doe" required />
                </li>

                <li>
                    <label for="email">Email:</label>
                    <input type="email" name="email" placeholder="john_doe@example.com" required />
                    <span class="form_hint">Proper format "name@something.com"</span>
                </li>

                <li>
                    <label for="message">Message:</label>
                    <textarea name="message" cols="40" rows="6" required ></textarea>
                </li>

                <li>
                    <button class="submit" type="submit">Submit Form</button>
                </li>
            </ul>
        </form>
    </div>
@endsection
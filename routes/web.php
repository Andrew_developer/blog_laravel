<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'MainController@index')->name('main');

Route::get('/post-view/{id}', 'PostController@view')->name('post');

Route::get('/page/{id}', 'PageController@view')->name('page');

Route::post('/comments-save', 'CommentController@commentSave')->name('comments.save');

Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');

Route::get('/contact', 'ContactController@index')->name('contact');

Route::post('/contact-save', 'ContactController@contactSave')->name('contact.save');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/registration', 'Auth\RegisterController@registration')->name('register');

Route::post('/user-save', 'Auth\RegisterController@create')->name('create.user');

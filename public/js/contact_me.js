// Contact Form Scripts

$(function() {

    $("#contactForm input, #contactForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            $form.preventSubmit = true;
        },
        submitSuccess: function($form, event) {

            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var message = $("textarea#message").val();
            var firstName = name;

            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $('.link-reply').click(function (e) {
        e.preventDefault();
        $(this).parent().next().fadeIn(200);
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});



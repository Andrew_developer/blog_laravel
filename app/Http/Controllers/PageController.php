<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function view(Request $request, $id){
        $page = \App\Page::where('slug', '=', $id)->first();

        return view('page', ['page' => $page]);
    }
}

@extends('layout.layout')

@section('content')
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <p>
            {!! $page->description !!}
        </p>
    </div>
@endsection
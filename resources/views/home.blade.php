@extends('layout.layout')

@section('content')
    @foreach($posts as $post)
        <div class="post-preview">
            <a href="{{route('post', ['id' => $post->slug])}}">
                <h2 class="post-title">
                    {{ $post->title }}
                </h2>

                <h3 class="post-subtitle">
                    {!! str_limit($post->description, 100) !!}
                </h3>
            </a>
            <p class="post-meta">Posted on {{$post->created_at->format('d/m/Y')}}</p>
        </div>
        <hr>
    @endforeach
@endsection
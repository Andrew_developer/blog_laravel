<div class="comments">
    <div>
        @foreach($comments as $comment)
            <div class="comment display-comment">
                <span>Author: </span> {{$comment->author}}
                <br>
                <span>Comment: </span> {{$comment->text}}

                <div>
                    <a class="link-reply" href="#">Reply</a>
                </div>

                <form class="contact_form reply" action="{{ route('reply.add') }}" method="post" name="contact_form">
                    @csrf
                    <ul>
                        <li>
                            <span>Add Comments</span>
                            <span class="required_notification">* Denotes Required Field</span>
                        </li>

                        <input name="id" type="hidden" value="{{ $post->id }}">

                        <input name="comment_id" type="hidden" value="{{ $comment->id }}">

                        <li>
                            <label for="name">Name:</label>
                            <input name="author" type="text" placeholder="John Doe" required/>
                        </li>

                        <li>
                            <label for="email">Email:</label>
                            <input type="email" name="email" placeholder="john_doe@example.com" required/>
                            <span class="form_hint">Proper format "name@something.com"</span>
                        </li>

                        <li>
                            <label for="message">Message:</label>
                            <textarea name="message" cols="40" rows="6" required></textarea>
                        </li>

                        <li>
                            <button class="submit" type="submit">Submit Form</button>
                        </li>
                    </ul>
                </form>

                @include('partials._comment_replies', ['comments' => $comment->replies])
            </div>
        @endforeach
    </div>
</div>

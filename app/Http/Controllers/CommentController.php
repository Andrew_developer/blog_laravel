<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function commentSave(Request $request){
        $comment = new \App\Comment();

        $comment->author = $request->author;
        $comment->email = $request->email;
        $comment->text = $request->message;

        $post = \App\Post::find($request->id);

        $res = $post->comments()->save($comment);

        if($res){
            return redirect()->back()->with(['msg', 'New comment has been added']);
        }
        else{
            return redirect()->back()->with(['msg', 'Error']);
        }
    }

    public function replyStore(Request $request)
    {
        $reply = new \App\Comment();
        $reply->author = $request->get('author');
        $reply->text = $request->get('message');
        $reply->email = $request->get('email');
        $reply->parent_id = $request->get('comment_id');

        $post = \App\Post::find($request->get('id'));
        $post->comments()->save($reply);

        return back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){
        return view('contact');
    }

    public function contactSave(Request $request){
        $contact = new \App\Contact();

        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;

        $res = $contact->save();

        if($res){
            return redirect()->back()->withMessage('New message has been added!');
        }
        else{
            return redirect()->back()->withMessage('Error');
        }
    }
}

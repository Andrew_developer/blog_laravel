<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MainController extends Controller
{
    public function index(){
        $posts = \App\Post::with('category')->with('comments')->get();

        return view('home', ['posts' => $posts]);
    }
}

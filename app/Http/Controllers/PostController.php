<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

class PostController extends Controller
{
    public function view(Request $request, $id){
        $post = \App\Post::with(['comments' => function($q) use($id) {
            $q->where('comments.parent_id', '=', Null);
        }])->where('slug', '=', $id)->first();

        return view('post', ['post' => $post]);
    }
}

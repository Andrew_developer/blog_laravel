@extends('layout.layout')

@section('content')
    <h1>Contact Me</h1>

    <p>
        Want to get in touch with me? Fill out the form below to send me a message and I will try to get back to you
        within 24 hours!
    </p>

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <form name="sentMessage" action="{{ route('contact.save') }}" method="post" id="contactForm" novalidate>
        @csrf

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Name</label>
                <input name="name" type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
                <p class="help-block text-danger"></p>
            </div>
        </div>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Email Address</label>
                <input name="email" type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
                <p class="help-block text-danger"></p>
            </div>
        </div>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Phone Number</label>
                <input name="phone" type="tel" class="form-control" placeholder="Phone Number" id="phone" required data-validation-required-message="Please enter your phone number.">
                <p class="help-block text-danger"></p>
            </div>
        </div>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Message</label>
                <textarea name="message" rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                <p class="help-block text-danger"></p>
            </div>
        </div>

        <br>

        <div id="success"></div>
        <div class="row">
            <div class="form-group col-xs-12">
                <button type="submit" class="btn btn-default">Send</button>
            </div>
        </div>
    </form>

@endsection